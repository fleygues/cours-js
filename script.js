
// EXO 4-2
(function(){
	
	var note, // Note que l'utiliser va rentrer
		nbNotes = 0, // Nombre de notes rentrées par l'utilisateur
		somme = 0, // Somme de toutes les notes pour calculer la moyenne
		moyenne, // On stock la moyenne calculée avec toutes les notes
		onContinue = true, // Permet de sortir de la boucle
		noteMax = 0;	// Enregistre la note max

	do {
		note = parseInt(prompt('Merci de renseigner la note N° ' + (nbNotes+1)));

		// On teste si l'utilisateur a renseigné une note comprise entre 0 et 20
		if((note <= 20) && (note >= 0)) {
			console.log('OK note comprise entre 0 et 20');
			somme = somme + note;
			nbNotes++;

			// SI la note rentrée par l'utilisateur est supérieur à la note maximale
			if (note > noteMax){
				noteMax = note; // La note maximale devient la note rentrée par l'utilisateur
			}

		}
		// SINON on sort de la boucle
		else {
			console.log('On arrête');
			onContinue = false;
		}

	} while(onContinue == true);

	moyenne = somme / nbNotes;
	console.log('La moyenne est de ' + moyenne); // Affiche la moyenne
 	console.log('La note maximale est de ' + noteMax); // Affiche la note maximale
 	console.log(nbNotes + ' notes ont été rentrées'); // Affiche le nombre de notes

});

// EXO 4-3
(function() {

	var note, // La note renseignée par l'utilisateur
		nbNotes, // Le nb de notes que l'utilisateur doit renseigner
		somme = 0, // Somme de toutes les notes pour calculer la moyenne
		onContinue, // Permet de sortir de la boucle do/while
		exitScript = false, // Permet de sortir de la boucle for
		noteMax = 0; // Permet de stocker la note maximale

	nbNotes = parseInt(prompt('Merci de renseigner le nombre de notes que vous souhaitez :'));

	// On fait nbNotes tour(s) de boucle
	for (var compteur = 0; compteur < nbNotes; compteur++) {

		// Si l'utilisateur souhaite arrêter le script
		if (exitScript == true) {
			break; // On sort de la boucle for
		}

		onContinue = true; // On initialise à chaque tour de boucle la variable d'arrêt de la boucle do/while

		do {
			note = parseInt(prompt('Merci de renseigner la note N°' + (compteur+1) ));

			// On vérifie que la note est bien comprise entre 0 et 20
			if (note >= 0 && note <= 20) {
				//console.log('OK : note comprise entre 0 et 20');
				somme = somme + note; // Autre façon de faire : somme += note;
				if (note > noteMax) {
					noteMax = note;
				}
				onContinue = false; // On sort de la boucle do/while qui permet de forcer de rentrer une valeur entre 0 et 20
			}
			else {
				exitScript = confirm('Souhaitez-vous arrêter ?'); // On demande à l'utilisateur si il souhaite arrêter avant de rentrer toutes les notes
				if (exitScript == true) {
					onContinue = false; // On sort de la boucle do/while qui permet de forcer de rentrer une valeur entre 0 et 20
					compteur--; // On décrémente le compteur car nous n'avons pas rentré de note sur ce tour de boucle
				}
				else { // L'utilisateur souhaite rester dans la boucle
					alert('Merci de renseigner une note comprise entre 0 et 20');
				}
			}	
		} while(onContinue == true);

	}

	moyenne = somme / compteur;

	console.log('Nombre de note demandées : ' + nbNotes);
	console.log('Nombre de note rentrées : ' + compteur);
	console.log('Le total des notes rentrées : ' + somme);
	console.log('La moyenne : ' + moyenne);
	console.log('La note max est : ' + noteMax);

})();

